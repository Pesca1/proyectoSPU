#include <EEPROM.h>
void setup(){
  EEPROM.begin(10);
  Serial.begin(115200);
  bool b[8];
  int val = (int)EEPROM.read(0);
  Serial.printf("\n Valor: %d", val);
  Serial.printf("\n Array:");
  for(int i= 7; i >= 0; i--){
  	b[i]= (val % 2 == 1);
  	val/=2;
  }
  for(int j= 0; j < 8; j++){
    Serial.printf(" %d", b[j]);
  }
}

void loop(){}