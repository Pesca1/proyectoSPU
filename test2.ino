#include <EEPROM.h>
void setup(){
  EEPROM.begin(10);
  Serial.begin(115200);
  bool b[8];
  int val = (int)EEPROM.read(0);
  Serial.printf("\n Valor: %d", val);
  Serial.printf("\n Array:");
  for(int i= 0; i < 8; i++){
  	b[i]= (val % 2 == 0);
  	Serial.printf(" %d", (b[i])?0:1);
  	val/=2;
  }
}

void loop(){}