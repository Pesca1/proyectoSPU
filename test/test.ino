#include <EEPROM.h>
void setup(){
  EEPROM.begin(10);
  Serial.begin(115200);
  bool b[8] = {true, false, false, true, false, true, false, true};
  Serial.printf("\n Array:");
  for(int i= 0; i < 8; i++){
  	Serial.printf(" %d", (b[i])?1:0);
  }

  int val = 0;
  for(int j= 0; j < 8; j++){
  	val*=2;
  	val+= (b[j])?1:0;
  }
  Serial.printf("\n Val: %d", val);
  EEPROM.write(0, val);
  EEPROM.commit();
}

void loop(){}