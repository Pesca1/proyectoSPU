#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Time.h>
#include <TimeLib.h>
#include <TimeAlarms.h>
#include <SPU.h>;
#include <DHT11.h>
#include <math.h>


// Pines predeterminados para sensores
#define PIR_PIN D7
#define RELAY1_PIN D6
#define RELAY2_PIN D5
#define CURRENT_PIN A0 
#define TEMPERATURE_PIN D4

// Direcciones de memoria
#define IP_DIR 0
#define SENSOR_DIR 4
#define PIR_DIR 0
#define RELAY1_DIR 1
#define RELAY2_DIR 2
#define TEMP_DIR 3

// Tiempo entre cada lectura de los sensores
#define SENSORS_UPDATE_TIME 5

// Tiempo para reenviar el mensaje si no hubo respuesta del maestro
#define RESEND_TIME 5

// Variables de la conexion
IPAddress masterIp(192, 168, 4, 1); // Ip del nodo maestro
IPAddress nodeIp; // Ip del esclavo, otorgada por el maestro
IPAddress subnet(255, 255, 255, 0);
IPAddress gateway(192, 168, 4, 1);
char ssid[] = "node1";
WiFiUDP udp;
unsigned int port = 4000;

// Alarma para reenviar mensaje a master en caso de no haber respuesta
int resendAlarm = ALARM_NOT_SET;

// Variables para envio de mensajess
int receivedMsgNumber = 1; //Último número de mensaje recibido desde master
int sentMsgNumber = 0; //Último número de mensaje enviado a master
int sentMsg; //Último mensaje enviado a master
int sentPacket[MAX_PACKET_SIZE]; // Último paquete enviado a master
int sentPacketSize;
bool packetSent;
int masterState = RUNNING_STATE; // Estado del maestro, no se si es necesario

// Variables de sensores
bool lightsOn = false;
DHT11 dht11(TEMPERATURE_PIN);
int pirPin = DISCONNECTED;
int relayPin1 = DISCONNECTED;
int relayPin2 = DISCONNECTED;
int currentSensorPin = DISCONNECTED;
int temperatureSensorPin = DISCONNECTED;
volatile int pirState; // Estado del PIR
float sensibility = 0.172; //sensibilidad en Volt/Amper para sensor de 5A
const int phicosine = 1; // desfasaje

// Datos de sensores
int temperature;
short int humidity;
float current;

void setup() {
  Serial.begin(115200);
  for(int i = 0; i < 50; i++) Serial.println();
  udp.begin(4000);
  EEPROM.begin(512);
  setTime(0, 0, 0, 1, 1, 2000);
  // Lee IP de EEPROM o pide una al maestro
  initConnection();
  
  initSensors(); //Inicializa sensores

  Alarm.timerRepeat(5, printTime);
}

void loop() { // Espera paquetes, controla la conexion y el pir
  checkConnection();
  if(udp.parsePacket()){
    handlePacket(); 
  }
  checkPIR();
  serialEvent();
  Alarm.delay(0);
}

// Recibe el paquete y decide que hacer
// Aca deberiamos decidir cuando es un mensaje (longitud de paquete = 2)
// o cuando es un paquete de datos (longitud > 2) y hacer algo al respecto
void handlePacket(){
  byte incomingPacket[MAX_PACKET_SIZE];
  int len = udp.read(incomingPacket, MAX_PACKET_SIZE);
  if (len == 2){
    handleMessage((int)incomingPacket[0]);
    receivedMsgNumber = (int)incomingPacket[1];
  }
}

// Maneja los mensajes
// Si llega un mensaje pero todavía se esta esperando respuesta, se ignora? se reenvia el mensaje anterior?
void handleMessage(int code){
  printTime();
  Serial.printf("\n Message: %d", code);
  if(masterState == PENDING_RESPONSE_STATE){
    masterState = RUNNING_STATE;
    Alarm.free(resendAlarm);
    resendAlarm = ALARM_NOT_SET;
    if(code == RECEIVED_MSG) return;
    
  }
  switch(code){
    case STATE_MSG:
      Serial.printf("\nSending state to master");
      sendMessage(RUNNING_STATE);
      break;
    case LIGHTS_ON_MSG:
      turnLightsOn();
      //storeActiveSensor(1,5);
      Serial.printf("\nLights on");
      lightsOn = true;
      sendAck();
      break;
    case LIGHTS_OFF_MSG:
      turnLightsOff();
      //deleteActiveSensor(1,6);
      Serial.printf("\nLights off");
      lightsOn = false;
      sendAck();
      break;
    case CONNECT_PIR_MSG:
      initPir();
      writeSensors();
      Serial.printf("\nPIR on");
      sendAck();
      break;
    case DISCONNECT_PIR_MSG:
      disconnectPir();
      writeSensors();
      //deleteActiveSensor(2,8);
      Serial.printf("\nPIR off");
      sendAck();
      break;
    case CONNECT_TEMPERATURE_MSG:
      initTemperatureSensor();
      writeSensors();
      //storeActiveSensor(3,9);
      Serial.printf("\nTemperature sensor on");
      sendAck();
      break;
    case DISCONNECT_TEMPERATURE_MSG:
      disconnectTemperatureSensor();
      writeSensors();
      //deleteActiveSensor(3,10);
      Serial.printf("\nTemperature sensor off");
      sendAck();
      break;
    case CONNECT_RELAY1_MSG:
      initRelay(1);
      writeSensors();
      Serial.printf("\nRelay 1 on");
      sendAck();
      break;
    case DISCONNECT_RELAY1_MSG:
      disconnectRelay(1);
      writeSensors();
      Serial.printf("\nRelay 1 off");
      sendAck();
      break;
    case CONNECT_RELAY2_MSG:
      initRelay(2);
      writeSensors();
      Serial.printf("\nRelay 2 on");
      sendAck();
      break;
    case DISCONNECT_RELAY2_MSG:
      disconnectRelay(2);
      writeSensors();
      Serial.printf("\nRelay 2 off");
      sendAck();
      break;
    case SENSOR_DATA_MSG:
      Serial.printf("\n Sending sensor data to master");
      sendSensorData();
      break;
    case RECEIVED_MSG:
      Serial.printf("\n RECIBIDO: %s", udp.remoteIP().toString().c_str());
      break;
    default:
      Serial.printf("\nUnknown message code received: '%i'", code);
      sendAck();
      break;
  }
}

// Realiza la conexion con el maestro
void connectToMaster(){
  int message;
  while(true){
    Serial.printf("\nConnecting to %s\nSending first request\n", masterIp.toString().c_str());
    sendMessage(CONNECTION_MSG);
    Serial.printf("\nWaiting for response");
    int i = 0;
    while(!udp.parsePacket()){
      Serial.printf(".");
      delay(500);
      i++;
      if(i == 10) reset();
    }
    message = (int)udp.read();
    //Serial.printf("\nReceived: %d", message);
    if(message == RECEIVED_MSG){
      masterState = RUNNING_STATE;
      Serial.printf("\nConected to master");
      return;
    }
  }
}

// Conecta a la red del maestro, configurando antes la ip estatica
void connectWifi(IPAddress ip){
  WiFi.config(ip, gateway, subnet);
  connectWifi();
}

// Conecta a la red del maestro por DHCP
void connectWifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid);
  Serial.println("Connecting");
  int i = 0;
  while(WiFi.status() != WL_CONNECTED){
    delay(1000);
    Serial.print(".");
    i++;
    if(i == 20) {
      reset();
    }  
  }
  Serial.printf("\n Wifi connected.");
}

// Envía un mensaje de respuesta
bool sendAck(){
  noInterrupts();
  udp.beginPacket(masterIp, port);
  udp.write((byte)RECEIVED_MSG);
  udp.write((byte)sentMsgNumber);
  bool result = udp.endPacket();
  yield();
  sentMsgNumber++;
  sentMsg = RECEIVED_MSG;
  interrupts();
  return result;
}

// Controla que la conexion no se haya perdido y la restaura
void checkConnection(){
  if(WiFi.status() != WL_CONNECTED){
    Serial.printf("\nConnection lost.");
    WiFi.reconnect();
    int i= 0;
    while(WiFi.status() != WL_CONNECTED){
      delay(1000);
      Serial.print(".");
      i++;
      if(i == 20) reset();
    }
    connectToMaster();
    Serial.printf("\nConnection restored.");
  }
}

// Conecta con el maestro y pide IP si es necesario
void initConnection(){
  nodeIp = storedIp();
  if(nodeIp[0] != 0){
    Serial.printf("\nIP: %s", nodeIp.toString().c_str());  
  } else {
    connectWifi();
    connectToMaster();
    sendMessage(IP_MSG);
    Serial.printf("\nRequesting new IP.");
    while(!udp.parsePacket())delay(500);
    byte packet[4];
    udp.read(packet, 4);
    IPAddress newIp((int)packet[0], (int)packet[1], (int)packet[2], (int)packet[3]);
    nodeIp = newIp;
    storeIp(nodeIp);
    Serial.printf("\nNew IP: %s", nodeIp.toString().c_str());
    WiFi.disconnect(true);
  }
  connectWifi(nodeIp);
  connectToMaster();
}

// Envía la informacion de los sensores al maestro
void sendSensorData(){
  int packet[MAX_PACKET_SIZE];
  int length = 0;
  if(temperatureSensorPin != DISCONNECTED){
    packet[length] = temperature;
    length++;
    packet[length] = humidity;
    length++;
  }
  if(currentSensorPin != DISCONNECTED){
    unsigned short int c = static_cast<unsigned short int>(current*1000);
    packet[length] = c >> 8;
    length++;
    packet[length] = c - (c >> 8);
    length++;
  }
  sendPacket(packet, length);
}

// Lee la ip almacenada
IPAddress storedIp(){
  int n[4];
  for(int i = IP_DIR; i <= 3; i++){
    n[i]= (int)EEPROM.read(i);
  }
  IPAddress ip(n[0], n[1], n[2], n[3]);
  return ip;
}

// Almacena la ip nueva
void storeIp(IPAddress ip){
  for(int i= IP_DIR; i < 4; i++){
    EEPROM.write(i, ip[i]);
  }
  EEPROM.commit();
}

// Envia un mensaje, incrementa el numero de mensaje
void sendMessage(int code){
  udp.beginPacket(masterIp, port);
  udp.write((byte)code);
  udp.write((byte)sentMsgNumber);
  udp.endPacket();
  yield();
  sentMsgNumber++;
  sentMsg = code;
  masterState = PENDING_RESPONSE_STATE;
  packetSent = false;
  resendAlarm = Alarm.timerOnce(RESEND_TIME, resendLastMessage);
}

// Envia un paquete de datos
int sendPacket(int* packet, int len) {
  Serial.printf("\n Sending packet. length: %d", len);
  udp.beginPacket(masterIp, port);
  for(int i = 0; i < len; i++){
    udp.write((byte)packet[i]);  
  }
  udp.write((byte)sentMsgNumber);
  if(len == 1){ // Si el paquete es muy pequeño se llena para que el maestro lo identifique como paquete
    udp.write((byte)0);
  }
  int result = udp.endPacket();
  yield();
  sentMsgNumber++;
  for(int i = 0; i < MAX_PACKET_SIZE; i++){
    sentPacket[i] = packet[i]; 
  }
  masterState = PENDING_RESPONSE_STATE;
  packetSent = true;
  sentPacketSize = len;
  resendAlarm = Alarm.timerOnce(RESEND_TIME, resendLastMessage);
  return result;
}

void resendLastMessage(){
  if(packetSent){
    sendPacket(sentPacket, sentPacketSize);
  } else {
    sendMessage(sentMsg);
  }
}

// Notifica que se detectó movimiento
void detectMotion(){
  pirState = HIGH;
}

// Hace cualquier checkeo necesario para sensores
void checkSensors(){
  if(currentSensorPin != DISCONNECTED){
    checkCURRENT();
  }
  if(temperatureSensorPin != DISCONNECTED){
    checkTEMPERATURE();
  }
}

// Si se detectó movimiento, envia mensaje al maestro
void checkPIR(){
  if((pirPin != DISCONNECTED) && (pirState == HIGH)){
    sendMessage(MOTION_DETECTED_MSG);
    printTime();
    Serial.printf("\nMotion detected");
    pirState = LOW;
  }
}

// Inicializa el PIR
void initPir(){
  pirPin = PIR_PIN;
  pirState = digitalRead(PIR_PIN);
  attachInterrupt(digitalPinToInterrupt(PIR_PIN), detectMotion, FALLING);
}

// 'Desconecta' el PIR
void disconnectPir(){
  pirPin = DISCONNECTED;
  detachInterrupt(PIR_PIN);
}

// Inicializa el sensor de temperatura
void initTemperatureSensor(){
  temperatureSensorPin = TEMPERATURE_PIN;
}

// desconecta el sensor de temperatura
void disconnectTemperatureSensor(){
  temperatureSensorPin = DISCONNECTED;
}

//inicializa el sensor de corriente
void initCurrentSensor(){
  currentSensorPin = CURRENT_PIN;
}

// desconecta el sensor de corriente
void disconnectCurrentSensor(){
  currentSensorPin = DISCONNECTED;
}

// Inicializa el relay que se pasa por argumento
void initRelay(int relay){
  switch(relay){
    case 1:
      relayPin1 = RELAY1_PIN;
      break;
    case 2:
      relayPin2 = RELAY2_PIN;
      break;
  }
  if(lightsOn) turnLightsOn();
}

// Desconecta el relay que se pasa por argumento
void disconnectRelay(int relay){
  switch(relay){
    case 1:
      relayPin1 = DISCONNECTED;
      digitalWrite(RELAY1_PIN, HIGH);
      break;
    case 2:
      relayPin2 = DISCONNECTED;
      digitalWrite(RELAY2_PIN, HIGH);
      break;
  }
}

// Inicializa los sensores
void initSensors(){
  pinMode(PIR_PIN, INPUT_PULLUP);
  pinMode(RELAY1_PIN, OUTPUT);
  pinMode(RELAY2_PIN, OUTPUT);
  digitalWrite(RELAY1_PIN, HIGH);
  digitalWrite(RELAY2_PIN, HIGH);
  Alarm.timerRepeat(SENSORS_UPDATE_TIME, checkSensors);
  readSensors();
}

// Lee los sensores conectados en la EEPROM
void readSensors(){
  bool b[8];
  int val = EEPROM.read(SENSOR_DIR);
  for(int i= 7; i >= 0; i--){
    b[i]= (val % 2 == 1);
    val/=2;
  }
  Serial.printf("\n Array: ");
  for(int i= 0; i < 8; i++){
    Serial.printf(" %d", (b[i])?1:0);
  }
  if(b[PIR_DIR]) initPir();
  if(b[TEMP_DIR]) initTemperatureSensor();
  if(b[RELAY1_DIR]) initRelay(1);
  if(b[RELAY2_DIR]) initRelay(2);
}

void writeSensors(){
  bool b[8];
  for(int i = 0; i < 8; i++) b[i]= false;
  if(pirPin != DISCONNECTED) b[PIR_DIR] = true;
  if(temperatureSensorPin != DISCONNECTED) b[TEMP_DIR] = true;
  if(relayPin1 != DISCONNECTED) b[RELAY1_DIR] = true;
  if(relayPin2 != DISCONNECTED) b[RELAY2_DIR] = true;
  int val = 0;
  Serial.printf("\n Array: ");
  for(int j= 0; j < 8; j++){
    val*=2;
    val+= (b[j])?1:0;
  }
  for(int i = 0; i < 8; i++) Serial.printf(" %d", (b[i])?1:0);
  EEPROM.write(SENSOR_DIR, val);
  EEPROM.commit();
}

// Prende las luces
void turnLightsOn(){
  if(relayPin1 != DISCONNECTED){
    digitalWrite(RELAY1_PIN, LOW);
  }
  if(relayPin2 != DISCONNECTED){
    digitalWrite(RELAY2_PIN, LOW);
  }
}

// Apaga las luces
void turnLightsOff(){
  if(relayPin1 != DISCONNECTED){
    digitalWrite(RELAY1_PIN, HIGH);
  }
  if(relayPin2 != DISCONNECTED){
    digitalWrite(RELAY2_PIN, HIGH);
  }
}

void checkCURRENT(){
  float corriente = get_corriente(200); // obtengo la corriente promedio de 200 muestras 
  Serial.printf("\n Corriente: ");
  Serial.print(corriente,3); // 3 digitos como maximo
  current = corriente;
}

// ------- CORRIENTE ALTERNA --------
float get_corriente(int n_sample){
  float voltageSensor;
  float current = 0; // reseteo la corriente para calcular entre las muestras
  float maximum = 0; // reseteo el maximo
  for(int i=0;i<n_sample;i++){
    voltageSensor = analogRead(A0)*(5.0/1023.0); //lectura del sensor    
    current=(voltageSensor-2.5)/sensibility; //Ecuación  para obtener la corriente
    if (current > maximum){ // calculo el valor pico
      maximum = current;
    }
  }
  current = maximum/1.41; // valor eficaz
  current = current*phicosine; 
  current = current+0.15; // factor de ajuste
  return(current); // corriente en amper
}

void checkTEMPERATURE(){
  int err;
  float temp;
  float humi;
  if((err=dht11.read(humi, temp))==0){
    Serial.printf("\n Temperature: ");
    Serial.print(temp);
    Serial.printf("\n Humidity: ");
    Serial.print(humi);
    temperature = static_cast<int>(temp);
    humidity = static_cast<short int>(humi);
  }else{ // deberiamos indicar algo al respecto?
    Serial.println();
    Serial.print("Error No :");
    Serial.print(err);
    Serial.println();    
  }
}

// Se ejecuta automaticamente con cada loop, lee el serial
void serialEvent() {
  bool stringComplete = false;
  String inputString = "";
  inputString.reserve(200);
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    if (inChar == '\n') {
      stringComplete = true;
      break;
    }
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
  }
  if (stringComplete){
    handleSerial(inputString);
  }
}

void handleSerial(String message){
  if(message == "CLEAR_EEPROM"){
    clearEEPROM();
    Serial.printf("\n EEPROM clear.");
  }
}

void clearEEPROM(){
  for (int i = 0; i < 512; i++)
    EEPROM.write(i, 0);
  EEPROM.commit();
}

void printTime(){
  // digital clock display of the time
  time_t t = now();
  Serial.println(); 
  Serial.print(hour(t));
  printDigits(minute(t));
  printDigits(second(t));
}

void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void reset(){
  Serial.printf("\nRESTARTING");
  delay(2000);
  ESP.reset();
}