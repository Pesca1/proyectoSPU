#include <EEPROM.h>
#include <Time.h>
#include <TimeLib.h>
#include <TimeAlarms.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "DS1302.h";
#include "compileTime.h";
#include <SPU.h>;
#include <SerialMenu.h>

#define BUTTON_PIN D0

#define MAX_NODES 8 //Numero maximo de nodos conectados
#define MOTION_DET_TIME 20 //Tiempo que permanecen prendidas las luces luego de que cese el movimiento
#define STATE_CHECK_TIME 15 // Tiempo para checkear el estado de los nodos
#define SENT_CHECK_TIME 5 // Tiempo cada cuanto se checkean los nodos que nos respondieron

// ---- RTC ---- //
/*#include <Wire.h>
#include <RtcDS3231.h>
#define countof(a) (sizeof(a) / sizeof(a[0]))
RtcDS3231<TwoWire> Rtc(Wire);*/

// Estructura que representa a cada uno de los nodos esclavos
typedef struct t_node{
  t_node(): lastMessageNumber(0), hasPirSensor(false), hasTemperatureSensor(false), hasCurrentSensor(false),
    timeSinceSent(0), lastPacketLength(0) {}
  
  IPAddress ip;
  int state;
  int lastMessageNumber;
  int lastMessage; //Ultimo mensaje enviado
  int lastPacket[MAX_PACKET_SIZE]; //Ultimo paquete enviado
  int lastPacketLength; // tamaño del ultimo paquete
  bool packetSent; // True si lo ultimo que se envio fue un paquete
  int timeSinceSent; // Periodos de SENT_CHECK_TIME segundos desde que se envio el paquete y no hay respuesta
  bool hasPirSensor;
  bool hasTemperatureSensor;
  bool hasCurrentSensor;
} slaveNode;

char ssid[] = "node1";
WiFiUDP udp;
unsigned int port = 4000;

int connectedNodes = 0; // Numero de nodos conectados
slaveNode nodes[MAX_NODES]; // Nodos conectados
int baseIp = 4; // IP a partir de la cual otorgar IPs a nodos, 192.168.4.4

bool lightsOn = false; //Indica si las luces de la habitación se encuentran prendidas
int motionAlarm = ALARM_NOT_SET; // Alarma para apagar las luces
bool buttonState;
bool motionDetected = false;

Menu menu;

void setup() {
  pinMode(BUTTON_PIN, INPUT);
  buttonState = digitalRead(BUTTON_PIN);
  Serial.begin(115200);
  EEPROM.begin(512);
  udp.begin(4000);
  for (int i = 0; i < 50; i++) Serial.println();
  
  Serial.print(WiFi.softAP(ssid) ? "SSID: node1." : "AP failed."); //
  Serial.printf("\nIP: %s\n", WiFi.softAPIP().toString().c_str());

  printEprom();
  baseIp = getBaseIp(); //Lee la memoria EEPROM en busca de la siguiente IP libre
  Serial.printf("\nIP BASE: %d", baseIp);

  DS1302_gettime();
  /*// ---- RTC Settings ----// 
  Rtc.Begin();
  RtcDateTime now = Rtc.GetDateTime(); // save date
  // never assume the Rtc was last configured by you, so
  // just clear them to your needed state
  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone); */

  
  Alarm.timerRepeat(STATE_CHECK_TIME, checkNodeStatus); // Alarma para controlar el estado de los nodos
  Alarm.timerRepeat(5, printActualTime);  // Imprime la hora
  Alarm.timerRepeat(SENT_CHECK_TIME, checkSentPackets); // Alarma para checkear si algun nodo no responde

  initMenu();
}

void loop() {
  int packetSize = udp.parsePacket(); 
  if (packetSize) { //Espera un paquete UDP
    handlePacket();
  }
  menu.serialEvent();
  checkButton();
  Alarm.delay(0);
}

// Recibe el paquete y decide que hacer
// Aca deberiamos decidir cuando es un mensaje (longitud de paquete = 2)
// o cuando es un paquete de datos (longitud > 2) y hacer algo al respecto
void handlePacket() {
  slaveNode* node;
  byte packet[MAX_PACKET_SIZE];
  int length = udp.read(packet, MAX_PACKET_SIZE);
  //Serial.printf("\nReceived %d from %s", message, udp.remoteIP().toString().c_str());
  if ( (node = getNode(udp.remoteIP())) ) {
    if(length == 2){
        int message = (int)packet[0];
        handleMessage(message, node);
    } else  {
      handleData(packet, node);
    }
  } else { 
    if((length == 2) && ((int)packet[0] == CONNECTION_MSG)){
      printActualTime();                                                 // Si es un nodo nuevo, lo registra, salvo que se haya llegado al maximo
      Serial.printf(" New node: %s", udp.remoteIP().toString().c_str()); // ¿Contemplar si un nodo nuevo envía un mensaje distinto del de conexion?
      if (connectedNodes < MAX_NODES) {
        nodes[connectedNodes].ip = udp.remoteIP();
        nodes[connectedNodes].state = RUNNING_STATE;
        connectedNodes++;
      } else
        Serial.printf("Maximun number of nodes reached.");
      sendMessage(RECEIVED_MSG, &nodes[connectedNodes-1]); // En cualquier caso responde el mensaje
    }
  }

}

// Maneja los mensajes de nodos conectados
void handleMessage(int message, slaveNode* node) {
  printActualTime();
  if(message == CONNECTION_MSG){ // Si un nodo conectado vuelve a enviar mensaje de conexion, sabemos que se reinició
    Serial.printf("\nNode %s has been reset.", node->ip.toString().c_str());
    sendMessage(RECEIVED_MSG, node);
    node->state = RUNNING_STATE;
    node->lastMessageNumber = 1;
    return;
  }
  if(message == IP_MSG){ // Si un nodo requiere IP nueva, se arma y envia. MODULARIZAR
    baseIp++;
    IPAddress newIp(192, 168, 4, baseIp);
    int *ip= (int*)malloc(sizeof(int)*4);
    for(int i= 0; i < 4; i++){
      ip[i]= newIp[i];
    }
    sendPacket(ip, 4, node);
    node->ip = newIp;
    storeIp(newIp);
    Serial.printf("\nNode requesting ip: %s", node->ip.toString().c_str());
    return;
  }
  switch(node->state){ // Decide dependiendo del estado. Modularizar?
    case PENDING_STATE: // Se espera el estado del nodo
      switch(message){
        case RUNNING_STATE:
          Serial.printf(" %s node is running.", node->ip.toString().c_str());
          node->state = message;
          break;
        case ERROR_STATE:
          Serial.printf(" ERROR IN NODE %s", node->ip.toString().c_str());
          node->state = message;
          break;
        case MOTION_DETECTED_MSG: motionDetected = true; // Tal vez deberían cambiar el estado a RUNNING??
      }
      sendMessage(RECEIVED_MSG, node);
      break;
    case PENDING_RESPONSE_STATE: // Se espera la respuesta de un mensaje
      if(message == RECEIVED_MSG){
        if(node->timeSinceSent > 0){
          Serial.printf(" Node %d responded", nodeId(node));
        }
        node->state = RUNNING_STATE;
        node->timeSinceSent= 0;
      }
      break;
    case RUNNING_STATE: // Si el nodo esta corriendo, se esperan los mensajes de sensores etc
      switch (message) {
        case MOTION_DETECTED_MSG:
          sendMessage(RECEIVED_MSG, node);
          motionDetected = true;
          Serial.printf("\n Motion detected");
          break;
        case RECEIVED_MSG:
          Serial.printf("\n Unexpected ACK");
          break;
        default:
          Serial.printf(" Unknown message from node %s", node->ip.toString().c_str());
          sendMessage(RECEIVED_MSG, node);
          break;
      }
      break;
  }
}

// Maneja los paquetes de datos de los nodos
void handleData(byte packet[], slaveNode * node){
  int position = 0; // Siguiente posicion que leer del ṕaquete
  switch(node->state){
    case PENDING_RESPONSE_STATE:
      // Formato del paquete
      // 1 byte de temperatura, se castea directamente a int
      // 2 bytes de corriente, se lee en short int, se divide por 1000 y castea a float
      Serial.printf("\nParsing packet from %s", node->ip.toString().c_str());
      if(node->hasTemperatureSensor){
        int temperature = (int)packet[position] ;
        Serial.printf("\n Temperature: %d", temperature);
        position++;
        short int h = (short int)packet[position];
        position++;
        Serial.printf("\n Humidity: %d", h);
      }
      if(node->hasCurrentSensor){
        unsigned short int c = (unsigned short int)packet[position] << 8;
        position++;
        c += (unsigned short int)packet[position];
        position++;
        float current = (static_cast<float>(c))/1000;
        Serial.printf("\n Current: %f", current);
      }
      node->state = RUNNING_STATE;
      break;
    default:
      Serial.printf("\nNon requested data received from node %s", node->ip.toString().c_str());
      break;
  }
  sendMessage(RECEIVED_MSG, node);
}

// Pide el estado de todos los nodos, cada 30 segundos
// Ignorar si se esta esperando una respuesta del nodo?
void checkNodeStatus() {
  int i ;
  for (i = 0; i < connectedNodes; i++) {
    if((nodes[i].state != PENDING_RESPONSE_STATE) && (nodes[i].state != ERROR_STATE)){
      sendMessage(STATE_MSG, &nodes[i]);
      nodes[i].state = PENDING_STATE;
      Serial.printf("\n Checking status for %s", nodes[i].ip.toString().c_str());
    } 
  }
}

void checkSentPackets(){
  slaveNode * node;
  for(int i = 0; i < connectedNodes; i++){
    node = &nodes[i];
    if(node->state == PENDING_RESPONSE_STATE){
      Serial.printf("\n Node %d not responding for %d", i, node->timeSinceSent);
      if(node->timeSinceSent > 0){
        if(node->timeSinceSent < 5){
          if(node->packetSent){
            sendPacket(node->lastPacket, node->lastPacketLength, node);
          } else {
            sendMessage(node->lastMessage, node);
          }
        } else if((node->timeSinceSent < 16) && (node->timeSinceSent % 2 == 0)){
          if(node->packetSent){
            sendPacket(node->lastPacket, node->lastPacketLength, node);
          } else {
            sendMessage(node->lastMessage, node);
          }
        } else if(node->timeSinceSent >= 16){
          node->state = ERROR_STATE;
        }
        Serial.printf("\n Resending last message for node %d", i);
      }
      node->timeSinceSent++;
    }
  }
}

// Devuelve el nodo con el ip que se pasa, o 0 si no es un nodo conectado
slaveNode * getNode(IPAddress ip) {
  for (int i = 0; i < connectedNodes; i++) {
    if (nodes[i].ip == ip) {
      return &nodes[i];
    }
  }
  return 0;
}

// Apaga las luces
void turnOffLights() {
  lightsOn = false;
  sendGlobalMessage(LIGHTS_OFF_MSG);
  Alarm.free(motionAlarm);
  motionAlarm = ALARM_NOT_SET;
  printActualTime();
  Serial.printf(" Turning off lights.");
}

// Prende las luces
void turnOnLights(){
  sendGlobalMessage(LIGHTS_ON_MSG);
  lightsOn = true;
  motionDetected = false;
  motionAlarm = Alarm.timerOnce(MOTION_DET_TIME, checkMotion);
}

// Pospone el apagado de las luces
void delayLightsOff(){
  Alarm.free(motionAlarm);
  motionDetected = false;
  motionAlarm = Alarm.timerOnce(MOTION_DET_TIME, turnOffLights);
  printActualTime();
  Serial.printf(" Lights turn off delayed");
}

// 
void checkMotion(){
  if(motionDetected){
    delayLightsOff();
  } else if(connectedPir()) {
    turnOffLights();
  }
}

// Controla el pulsador
void checkButton(){
  int state = digitalRead(BUTTON_PIN);
  if((state == HIGH) && !buttonState){
    if(lightsOn){
      Serial.printf("\n Off");
      turnOffLights();
    } else {
      Serial.printf("\n On");
      turnOnLights();
    }
    buttonState = true;
  } else if(state == LOW){
    buttonState = false;
  }
}

// COntrola que haya un pir conectado
bool connectedPir(){
  for(int i = 0; i < connectedNodes; i++){
    if(nodes[i].hasPirSensor)
      return true;
  }
  return false;
}

// Envia broadcast, se espera respuesta de todos los nodos
void sendGlobalMessage(int code) {
  for (int i = 0; i < connectedNodes; i++) {
    if((nodes[i].state != PENDING_STATE) && (nodes[i].state != PENDING_RESPONSE_STATE)){
      sendMessage(code, &nodes[i]);
      nodes[i].state = PENDING_RESPONSE_STATE;
      //delay(10);
    }
  }
}

// Esta conectado un nodo con esa ip?
// No se usa, borrar?
bool isNode(IPAddress ip) {
  for (int i = 0; i < connectedNodes; i++) {
    if (nodes[i].ip == ip) return true;
  }
  return false;
}

// Envia un mensaje a un nodo
// junto con numero de mensaje, que se incrementa
int sendMessage(int code, slaveNode * node) {
  udp.beginPacket(node->ip, port);
  udp.write((byte)code);
  udp.write((byte)node->lastMessageNumber);
  int result = udp.endPacket();
  node->lastMessageNumber++;
  node->lastMessage = code;
  node->packetSent = false;
  return result;
}

// Envia un paquete a un nodo
// junto con numero de mensaje, que se incrementa
int sendPacket(int* packet, int len, slaveNode * node) {
  udp.beginPacket(node->ip, port);
  for(int i = 0; i < len; i++){
    udp.write((byte)packet[i]);  
  }
  udp.write((byte)node->lastMessageNumber);
  int result = udp.endPacket();
  node->lastMessageNumber++;
  node->packetSent = true;
  node->lastPacketLength = len;
  for(int i = 0; i < MAX_PACKET_SIZE; i++){
    node->lastPacket[i] = packet[i]; 
  }
  return result;
}

// Imprime fecha y hora actuales
void printActualTime() {
  /*RtcDateTime now = Rtc.GetDateTime(); // save date
  printDateTime(now);*/
  digitalClockDisplay(now());
}

void digitalClockDisplay(time_t t){
  // digital clock display of the time
  Serial.println(); 
  Serial.print(hour(t));
  printDigits(minute(t));
  printDigits(second(t));
  Serial.print(" ");
  Serial.print(day(t));
  Serial.print(" ");
  Serial.print(month(t));
  Serial.print(" ");
  Serial.print(year(t)); 
}

void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}
/*void printDateTime(const RtcDateTime& dt){
    char datestring[20];
    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
            dt.Day(),
            dt.Month(),            
            dt.Year(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    Serial.printf("\n");
    Serial.print(datestring);
}*/

// Calcula la ultima ip que se otorgó a un nodo
// EFICIENCIA
int getBaseIp(){
  int ip, base= baseIp;
  for(int i = 0; i < 512; i++){
    ip = EEPROM.read(i);
    if(ip > base) base= ip;
  }
  return base;
}

// Almacena nueva ip
void storeIp(IPAddress ip){
  EEPROM.write(baseIp-4, ip[3]);
  EEPROM.commit();
}

// DEBUG: imprime contenido de la EEPROM
void printEprom(){
  for(int i = 0; i < 10; i++){
    Serial.printf("\nPosition %d: %d", i, EEPROM.read(i));  
  }  
}

// Devuelve el numero de nodo
int nodeId(slaveNode * node){
  for(int i = 0; i < connectedNodes; i++){
    if(node->ip == nodes[i].ip) return i;
  }
  return -1;
}


// Crea el menu serial
void initMenu(){
  menu.addEvent("EVENTS", printEvents);
  menu.addEvent("DATA",getSensorData);
  menu.addEvent("CONNECT_PIR", connectPir);
  menu.addEvent("DISCONNECT_PIR", disconnectPir);
  menu.addEvent("CONNECT_TEMP", connectTemp);
  menu.addEvent("DISCONNECT_TEMP", disconnectTemp);
  menu.addEvent("CONNECT_CURRENT", connectCurrent);
  menu.addEvent("DISCONNECT_CURRENT", disconnectCurrent);
  menu.addEvent("CONNECT_RELAY1", connectRelay1);
  menu.addEvent("DISCONNECT_RELAY1", disconnectRelay1);
  menu.addEvent("CONNECT_RELAY2", connectRelay2);
  menu.addEvent("DISCONNECT_RELAY2", disconnectRelay2);
  menu.addEvent("NODES", printNodeInfo);
  menu.addEvent("CLEAR_EEPROM", clearEEPROM);
  menu.addEvent("TIME", setRTCTime);
}

void printEvents(int argc, String args[]){
  menu.printEvents();
}

void connectPir(int argc, String args[]){
  Serial.printf("\n Connecting pir");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(CONNECT_PIR_MSG);
    for(int i = 0; i < connectedNodes; i++){
      nodes[i].hasPirSensor = true;
    }
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(CONNECT_PIR_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        nodes[node].hasPirSensor = true;
        Serial.printf("\n Connecting pir of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void disconnectPir(int argc, String args[]){
  Serial.printf("\n Disconnecting pir");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(DISCONNECT_PIR_MSG);
    for(int i = 0; i < connectedNodes; i++){
      nodes[i].hasPirSensor = false;
    }
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(DISCONNECT_PIR_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        nodes[node].hasPirSensor = false;
        Serial.printf("\n Disconnecting pir of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void connectTemp(int argc, String args[]){
  Serial.printf("\n Connecting temperature");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(CONNECT_TEMPERATURE_MSG);
    for(int i = 0; i < connectedNodes; i++){
      nodes[i].hasTemperatureSensor = true;
    }
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(CONNECT_TEMPERATURE_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        nodes[node].hasTemperatureSensor = true;
        Serial.printf("\n Connecting temperature of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void disconnectTemp(int argc, String args[]){
  Serial.printf("\n Disconnecting temperature");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(DISCONNECT_TEMPERATURE_MSG);
    for(int i = 0; i < connectedNodes; i++){
      nodes[i].hasTemperatureSensor = false;
    }
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(DISCONNECT_TEMPERATURE_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        nodes[node].hasTemperatureSensor = false;
        Serial.printf("\n Disconnecting temperature of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void connectCurrent(int argc, String args[]){
  Serial.printf("\n Connecting current");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(CONNECT_CURRENT_MSG);
    for(int i = 0; i < connectedNodes; i++){
      nodes[i].hasCurrentSensor = true;
    }
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(CONNECT_CURRENT_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        nodes[node].hasCurrentSensor = true;
        Serial.printf("\n Connecting current of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void disconnectCurrent(int argc, String args[]){
  Serial.printf("\n Disconnecting current");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(DISCONNECT_CURRENT_MSG);
    for(int i = 0; i < connectedNodes; i++){
      nodes[i].hasCurrentSensor = false;
    }
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(DISCONNECT_CURRENT_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        nodes[node].hasCurrentSensor = false;
        Serial.printf("\n Disconnecting current of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void getSensorData(int argc, String args[]){
  Serial.printf("\n Getting sensor data");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(SENSOR_DATA_MSG);
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(SENSOR_DATA_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        Serial.printf("\n Getting sensor data of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void connectRelay1(int argc, String args[]){
  Serial.printf("\n Connecting relay 1");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(CONNECT_RELAY1_MSG);
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(CONNECT_RELAY1_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        Serial.printf("\n Connecting relay 1 of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void connectRelay2(int argc, String args[]){
  Serial.printf("\n Connecting relay 2");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(CONNECT_RELAY2_MSG);
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(CONNECT_RELAY2_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        Serial.printf("\n Connecting relay 2 of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void disconnectRelay1(int argc, String args[]){
  Serial.printf("\n Disconnecting relay 1");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(DISCONNECT_RELAY1_MSG);
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(DISCONNECT_RELAY1_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        Serial.printf("\n Disconnecting relay 1 of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void disconnectRelay2(int argc, String args[]){
  Serial.printf("\n Disconnecting relay 2");
  if(args[0].equalsIgnoreCase("ALL")){
    sendGlobalMessage(DISCONNECT_RELAY2_MSG);
  } else if(args[0] != ""){
    int node = args[0].toInt();
    if(node < connectedNodes){
      if(isAvailable(nodes[node])){
        sendMessage(DISCONNECT_RELAY2_MSG, &nodes[node]);
        nodes[node].state = PENDING_RESPONSE_STATE;
        Serial.printf("\n Disconnecting relay 2 of node %d", node);
      } else 
        Serial.printf("\n Node %d is not available", node);
    } else Serial.printf("\n Node %d is not connected", node);
  }
}

void printNodeInfo(int argc, String args[]){
  Serial.printf("\n Nodes:");
  for(int i = 0; i < connectedNodes; i++){
    Serial.printf("\n  %d - IP: %s State: ", i, nodes[i].ip.toString().c_str());
    switch(nodes[i].state){
      case PENDING_STATE: Serial.printf("pending"); break;
      case RUNNING_STATE: Serial.printf("running"); break;
      case PENDING_RESPONSE_STATE: Serial.printf("pending response for %d seconds", nodes[i].timeSinceSent*SENT_CHECK_TIME); break;
      case ERROR_STATE: Serial.printf("error"); break;
    }
  }
}

void clearEEPROM(int argc, String args[]){
  for (int i = 0; i < 512; i++)
    EEPROM.write(i, 0);
  EEPROM.commit();
  Serial.printf("\n EEPROM clear.");
}

void setRTCTime(int argc, String args[]){
  if(argc != 6){
    Serial.printf("\n Enter hours, minutes, day of week, day of month, month, year");
  } else {
    Serial.printf("\n Enter hours %d, minutes %d, day of week %d, day of month %d, month %d, year %d", args[0].toInt(), args[1].toInt(),
      args[2].toInt(), args[3].toInt(), args[4].toInt(), args[5].toInt());
    DS1302_settime(args[0].toInt(), args[1].toInt(), 0,
      args[2].toInt(), args[3].toInt(), args[4].toInt(), args[5].toInt());
  }
}

// Retorna true si un nodo esta disponible para enviarle un mensaje
bool isAvailable(slaveNode node){
  return ((node.state != PENDING_STATE) && (node.state != PENDING_RESPONSE_STATE));
}